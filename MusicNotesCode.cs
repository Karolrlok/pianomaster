using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicNotesCode : MonoBehaviour
{
    public AudioSource C_note;
    public AudioSource D_note;
    public AudioSource E_note;
    public AudioSource F_note;
    public AudioSource G_note;
    public AudioSource A_note;
    public AudioSource H_note;
    public AudioSource Cis_note;
    public AudioSource Dis_note;
    public AudioSource Fis_note;
    public AudioSource Gis_note;
    public AudioSource Ais_note;

public void C_note_Play(){
    C_note.Play();
}

public void D_note_Play(){
    D_note.Play();
}

public void E_note_Play(){
    E_note.Play();
}

public void F_note_Play(){
    F_note.Play();
}

public void G_note_Play(){
    G_note.Play();
}

public void A_note_Play(){
    A_note.Play();
}

public void H_note_Play(){
    H_note.Play();
}

public void Cis_note_Play(){
    Cis_note.Play();
}

public void Dis_note_Play(){
    Dis_note.Play();
}

public void Fis_note_Play(){
    Fis_note.Play();
}

public void Gis_note_Play(){
    Gis_note.Play();
}

public void Ais_note_Play(){
    Ais_note.Play();
}
}
